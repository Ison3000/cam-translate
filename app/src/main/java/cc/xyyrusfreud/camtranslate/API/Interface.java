package cc.xyyrusfreud.camtranslate.API;

import cc.xyyrusfreud.camtranslate.MODEL.Model;
import retrofit2.Call;

import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Interface {
    @POST("/language/translate/v2")
    Call<Model> requestTranslate(@Query("q") String q, @Query("source") String source, @Query("target") String target, @Query("key") String apiKey);
}