package cc.xyyrusfreud.camtranslate;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import cc.xyyrusfreud.camtranslate.API.Api;
import cc.xyyrusfreud.camtranslate.MODEL.ErrorModel;
import cc.xyyrusfreud.camtranslate.MODEL.Model;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityTranslation extends AppCompatActivity implements View.OnClickListener {

    private Gson gson;
    private ErrorModel errorModel;

    Toolbar toolbar;
    EditText Et1;
    EditText Et2;
    Button btn1;


    String st, apiKey = "AIzaSyB1S0gJq7KqvsK3nFuLItExNwBIpFx0YPc";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translation2);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("CamTranslate");

        btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(this);
        Et1 = (EditText) findViewById(R.id.et1);
        Et2 = (EditText) findViewById(R.id.et2);
        Et2.setText("Fetching Translation");
        Bundle bundle = getIntent().getExtras();

        st = bundle.getString("Value");
        Et1.setText(st);
        requestTranslate();
    }

    @Override
    public void onClick(View view) {
    switch (view.getId()){
        case R.id.btn1:
            st = Et1.getText().toString();
            requestTranslate();
            break;
        default:
            //Do Nothing
            break;

    }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                edit();
                break;
            case R.id.back:
                back();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void edit() {
        Et1.setEnabled(true);
//        Et2.setEnabled(true);
    }

    public void back() {
        Intent intent = new Intent(this, ExtractActivity.class);
        Et1.getText().clear();
        Et2.getText().clear();
        startActivity(intent);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void requestTranslate(){
        if(isNetworkAvailable()){
            Api.getClient().requestTranslate(st, "en", "tl", apiKey).enqueue(new Callback<Model>() {
                @Override
                public void onResponse(Call<Model> call, Response<Model> response) {
                    if(response.isSuccessful()){
                        Log.e("aaaaa", "onResponse: " + response.body().getData().getTranslations().get(0).getTranslatedText());
                        Et2.setText(response.body().getData().getTranslations().get(0).getTranslatedText());
                    }
                    else {
                        try {
                            errorModel = new ErrorModel();
                            gson = new Gson();
                            errorModel = gson.fromJson(response.errorBody().string(), ErrorModel.class);
                            Et2.setText("Translation Failed.\nPlease try again.");
                        } catch (Exception e) {
                        }
                    }
                }

                @Override
                public void onFailure(Call<Model> call, Throwable t) {
                    Toast.makeText(ActivityTranslation.this, "Failed",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
        else{
            Et2.setText("No Internet Connection.\nPlease connect to translate message.");
        }

    }
}






