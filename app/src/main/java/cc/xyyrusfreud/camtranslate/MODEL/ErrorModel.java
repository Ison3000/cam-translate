package cc.xyyrusfreud.camtranslate.MODEL;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorModel {

    @SerializedName("cod")
    @Expose
    private Integer cod;
    @SerializedName("message")
    @Expose
    private String message;


    /**
     * No args constructor for use in serialization
     *
     */
    public ErrorModel() {
    }

    public ErrorModel(Integer cod, String message) {
        super();
        this.cod = cod;
        this.message = message;
    }

    public Integer getCod() {
        return cod;
    }

    public void setCod(Integer statusCode) {
        this.cod = cod;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String statusDesc) {
        this.message = message;
    }
}